from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class Partner(models.Model):

	_inherit = "res.partner"

	nip = fields.Char(string="NIP", size=10)

	@api.onchange('nip')
	def nip_change(self):
		if self.nip:
			panjang=len(self.nip)
			# if {KONDISI}
			# TRUE/FALSE
			# {varpembanding} {operator} {nilai}
			# panjang {==, !=, <, >, <=, >=}
			_logger.error(panjang)
			if panjang != 10:
				raise ValidationError("Panjang yang di input = %s.\nInput NIP tidak valid" % panjang)

			self.update({'function':'Guruuuuuuuuu'})
			# return {
			# 	'value':{
			# 		'function':"Guru"
			# 	}
			# }