from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class TrainingDaftarAbsensiJadwalKelas(models.Model):

	_name = "training.daftar.absensi.jadwal.kelas"
	_description = "Training Daftar Absensi Jadwal Kelas"
	
	absensi_id = fields.Many2one("training.absensi.jadwal.kelas", string="Absensi", required=True, ondelete="CASCADE", onupdate="CASCADE")
	siswa_id = fields.Many2one("training.siswa", string="siswa", ondelete="RESTRICT", onupdate="RESTRICT", required=True)
	status = fields.Selection([("alpha", "Alpha"), ("izin", "Izin"), ("hadir", "Hadir")], string="status", required=True)
	kelas_id = fields.Many2one("training.kelas", string="Kelas", ondelete="RESTRICT", onupdate="RESTRICT", required=True)
	jadwal_kelas_id = fields.Many2one("training.jadwal.kelas", ondelete="RESTRICT", onupdate="RESTRICT", Required=True)
