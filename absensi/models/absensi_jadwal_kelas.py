from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class TrainingAbsensiJadwalKelas (models.Model):

	_name = "training.absensi.jadwal.kelas"
	_description = "Training Absensi Jadwal Kelas"

	guru_id = fields.Many2one('res.partner', string="Guru", required=True, ondelete="RESTRICT", onupdate="RESTRICT")
	jam_dan_tanggal = fields.Datetime(string="Jam dan Tanggal", default=fields.Datetime.now(), required=True)
	status = fields.Selection([("draft", "Draft"), ("ok", "OK"), ("batal", "Batal")], string="Status", required=True)
	jadwal_absensi_daftar_kelas_ids = fields.One2many('training.daftar.absensi.jadwal.kelas', 'absensi_id', string="Jadwal Absensi Daftar Kelas")



