{
    'name' : 'Modul absensi',
    'depends': ['sekolah', 'contacts'],
    'description': """Modul Untuk latihan dengan sistem sekolah""",
    'category': 'training',
    'website': 'https://www.odoo.com/page/billing',
    'data': [


        'views/absensi_jadwal_kelas.xml',
        'views/daftar_absensi_jadwal_kelas.xml',
        'views/partner.xml'
        
    ],
    'demo': [
        
    ],
    'qweb': [
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    
}
