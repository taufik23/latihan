{
    'name' : 'Modul Sekolah',
    'depends': ['base'],
    'description': """Modul Untuk latihan dengan sistem sekolah""",
    'category': 'training',
    'website': 'https://www.odoo.com/page/billing',
    'data': [


        'views/kelas.xml',
        'views/siswa.xml',
        'views/mata_pelajaran.xml',
        'views/training_jadwal_kelas.xml'
    ],
    'demo': [
        
    ],
    'qweb': [
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    
}
