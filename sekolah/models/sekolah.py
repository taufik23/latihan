from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class TrainingKelas(models.Model):

	_name = "training.kelas"
	_description = "Training Kelas"

	_rec_name = "nama"

	nama = fields.Char(string="Kelas", required=True)
	siswa_ids = fields.One2many('training.siswa', 'id_kelas', string="Daftar Siswa")
	jadwal_ids = fields.One2many('training.jadwal.kelas', 'id_kelas', string="Daftar Mata Pelajaran")


	_sql_constraints = [
	    ('nama_uniq', 'unique (nama)', _('The nama must be unique !')),
	]




class TrainingSiswa(models.Model):

	_name = "training.siswa"
	_description = "Training Siswa"
	_rec_name = "nama"
	nama = fields.Char(string="Nama", required=True, size=30)
	nis = fields.Char(string="NIS", required=True, size=4)
	tempat_lahir = fields.Char(string="Tempat Lahir", required=False, size=30)
	tanggal_lahir = fields.Date(string="Tanggal Lahir", reqiured=True)
	status = fields.Selection([("baru", "Baru"), ("aktif", "Aktif"), ("keluar", "Keluar"), ("lulus", "Lulus")], string="Status", required=True, default="baru")
	id_kelas = fields.Many2one("training.kelas", string="Kelas", required=True, ondelete="RESTRICT", onupdate="RESTRICT")

	_sql_constraints = [
		('nis_uniq', 'unique (nis)', 'Sudah Ada Yang Punya!')
	]

	@api.multi
	def diterima(self):

		msg = "Menjalakankan Perintah diterima()"
		
		_logger.error(msg)

		ada_yang_non_baru = self.filtered(lambda r:r.status!='baru')
		if ada_yang_non_baru:
			raise ValidationError("Ada data yang status nya bukan \"baru\".")
		
		self.write({'status':'aktif'})


	@api.multi
	def di_do(self):

		msg = "Menjalankan Perintah di_do()"
		
		_logger.error(msg)
		
		ada_yang_non_aktif = self.filtered(lambda r:r.status!='aktif')
		if ada_yang_non_aktif:
			raise ValidationError("Ada data yang status nya bukan \"aktif\". Tidak bisa D.O Anak yang non aktif")

		self.write({'status':'keluar'})


	@api.multi
	def lulus(self):

		msg = "Menjalankan Peintah lulus()"
		
		_logger.error(msg)

		ada_yang_non_aktif = self.filtered(lambda r:r.status!='aktif')
		if ada_yang_non_aktif:
			raise ValidationError("Ada data yang status nya bukan \"aktif\". Tidak bisa D.o anak yang non aktif")
		
		self.write({'status':'lulus'})