from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class TrainingMataPelajaran(models.Model):

	_name = "training.mata.pelajaran"
	_description = "Training Mata Pelajaran"
	_rec_name = "nama"

	kelas_id = fields.Many2one("training.kelas", string="Kelas", ondelete="RESTRICT", onupdate="RESTRICT", required=True)
	nama = fields.Char(string="Nama", size=64, required=True)
	deskripsi = fields.Html(string="Deskripsi", required=False)
	active = fields.Boolean(string="Active", required=True, default=True)
	jadwal_ids = fields.One2many('training.jadwal.kelas', 'id_mata_pelajaran', string="Daftar Mata Pelajaran")
	status = fields.Selection([("draft", "Draft"), ("submited", "Submited")], string="Status", default="draft", required=True)


	_sql_constraints = [
	    ('nama_uniq', 'unique (nama,kelas_id)', _('Nama mata pelajaran sudah terdaftar dikelas tersebut !')),
	]


