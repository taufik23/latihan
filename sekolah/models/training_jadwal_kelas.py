from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class TrainingJadwalKelas (models.Model):

	_name = "training.jadwal.kelas"
	_description = "Training Jadwal Kelas"

	id_kelas = fields.Many2one("training.kelas", string="Kelas", ondelete="RESTRICT", onupdate="RESTRICT", required=True)
	id_mata_pelajaran= fields.Many2one("training.mata.pelajaran", string="Mata Pelajaran", onupdate="RESTRICT", ondelete="RESTRICT", required=True)
	hari = fields.Selection([("1", "Senin"), ("2", "Selasa"), ("3", "Rabu"), ("4", "Kamis"), ("5", "Jum'at"), ("6", "Sabtu")], string="Hari", required=True)
	urutan = fields.Integer(string="Urutan", default="1", required=True)
	status = fields.Selection(([("draft", "Draft"), ("submit", "Submit"),]), string="Status", required=True, default="draft")