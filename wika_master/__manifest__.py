{
    'name' : 'Wika Master',
    'depends': ['base'],
    'description': """Wika Master""",
    'category': 'Wika',
    'data': [

        'views/project.xml',
        'views/master_form.xml',
        'views/master_score.xml',
        'views/master_entity.xml',
        'views/master_entity_score.xml'
        
    ],
    'demo': [
        
    ],
    'qweb': [
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    
}