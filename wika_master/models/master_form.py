from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class MasterForm(models.Model):

	_name = "master.form"
	_description = "Master Form"

	name = fields.Char(string="Name", required=True)
	active = fields.Boolean(string="active", required=True)
	entity_ids = fields.One2many('master.entity', 'form_id', string="Entity", required=True)

	_sql_constraints = [
	    ('name_uniq', 'unique (name)', _('Nama Project Tersebut Sudah Terdaftar !')),
	]