from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class MasterEntityScore(models.Model):

	_name = "master.entity.score"
	_description = "Master Entity Score"

	entity_id = fields.Many2one('master.entity', string="Entity ID", required=True)
	score_id = fields.Many2one('master.score', string="Score ID", required=True)
	description = fields.Text(string="Description", required=True)