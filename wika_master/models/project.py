from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class Project(models.Model):

	_name = "project"
	_description = "Project"

	name = fields.Char(string="Name", required=True)
	description = fields.Char(string="Description", required=True)
	active = fields.Boolean(string="Active", required=True)

	_sql_constraints = [
	    ('name_uniq', 'unique (name)', _('Nama Project tersebut sudah terdaftar !')),
	]