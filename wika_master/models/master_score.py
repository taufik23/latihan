from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class MasterScore(models.Model):

	_name = "master.score"
	_description = "Master Score"

	name = fields.Char(string="Name", required=True)
	value = fields.Float(string="Value", required=True)
	active = fields.Boolean(string="Active", required=True)
	sequence =fields.Integer(string="Sequence", required=True)