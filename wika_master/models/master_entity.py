from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError, ValidationError

import logging

_logger = logging.getLogger(__name__)

class MasterEntity(models.Model):

	_name = "master.entity"
	_description = "Master Entity"

	name = fields.Char(string="Name", required=True)
	parent_id = fields.Many2one(string="Parent", required=True)
	active = fields.Boolean(string="Active", required=True)
	parent_left = fields.Integer(string="Parent Left", required=True)
	parent_right = fields.Integer(string="Parent Right", required=True)
	sequence = fields.Integer(string="Sequence", required=True)
	form_id = fields.Many2one('master.form', string="Form ID", required=True)
	extra = fields.Boolean(string="Extra", default=False, required=False)
	child_ids = fields.One2many('master.entity', 'parent_id', string="Child", required=True)
	entity_score_ids = fields.One2many('master.entity.score', 'entity_id', string="Entity Score", required=True)